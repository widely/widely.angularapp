﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace webapi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("frontapi/[controller]")]
    public class TestLoginController : ControllerBase
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private string GetUserName() => _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        private string GetRole() => _httpContextAccessor.HttpContext.User.FindFirstValue("RoleDesc");
        private string GetToken() => _httpContextAccessor.HttpContext.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

        public TestLoginController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("GetUser")]
        public bool Get()
        {
            var accessToken = GetToken();
            var user = GetUserName();
            var getRoleDesc = GetRole();
            return true;
        }

    }
}
