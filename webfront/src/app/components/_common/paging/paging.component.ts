import { Component, OnInit,Input ,Output,EventEmitter  } from '@angular/core';
import { GridCriteria } from '../../../_models/gridcriteria';

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.css']
})
export class PagingComponent implements OnInit {

  @Input() gridCriteria: GridCriteria = {
    page:1,
    pageSize:20,
    totalPage:0,
    totalRecord:0,
    sortby:'',
    sortdir:''
   };
   @Output() gridBindingEvent = new EventEmitter<void>();
   gridBinding(): void {
    this.gridBindingEvent.next();
   }

   pages: number[];

  constructor() { 
    
  }

  getStartPage() :number { 
    return ((this.gridCriteria.page - 1) - (this.gridCriteria.page - 1) % 10) + 1;
  }

  getEndPage() :number { 
    let endpage:number = this.getStartPage()+9 ; 
    endpage = endpage > this.gridCriteria.totalPage ? this.gridCriteria.totalPage : endpage;
    return endpage;
   };

  getPageArray():number[]{ 
    let result:number[] = [];
    for (let i =  this.getStartPage() ; i <= this.getEndPage() ; i++)
    {
      result.push(i);
    }
    console.log(result);
    return result;
  }


  goPage(page: number) {
    this.gridCriteria.page = page;
    this.gridBinding();
    //console.log(page);
  }

  goPreviousPage()
  {
    this.gridCriteria.page--;
    this.gridBinding();
    //console.log(this.gridCriteria.page);
  }

  goNextPage()
  {
    this.gridCriteria.page++;
    this.gridBinding();
    //console.log(this.gridCriteria.page);
  }

  ngOnInit(): void {
  }

}
