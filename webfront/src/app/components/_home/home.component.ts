import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

import { User } from '../../_models';
import { UserService, AuthenticationService } from '../../_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = true;
    user: User;

    constructor(
        private userService: UserService,
        private authenticationService: AuthenticationService
    ) { }

    public get currentUserValue() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {
        // this.user = this.currentUserValue.data;
        this.user = this.authenticationService.currentUserValue;
        this.loading = false;
    }
}