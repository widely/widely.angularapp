import { NgModule,Component, OnInit,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RoleService } from '../../../_services/app_permission/role.service';
import { User } from '../../../_models';
import { AuthenticationService } from '../../../_services';
import { RoleModel } from '../../../_models/app_permission/role-model';
import { GridCriteria } from '../../../_models/gridcriteria';

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})

export class RoleComponent implements OnInit {

  Keyword:string;
  gridCriteria: GridCriteria = {
    page:1,
    pageSize:10,
    totalPage:0,
    totalRecord:0,
    sortby:'',
    sortdir:''
   };


  list: RoleModel[];
  user: User;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private rolesService: RoleService,
  ) { 

    
  }

  ngOnInit(): void {  
    this.gridBinding();
  }

  filter():void{
    this.gridCriteria.page=1;
    this.gridBinding();
  }
  gridBinding():void {
    console.log(this.Keyword);
    this.rolesService.getData(this.Keyword, this.gridCriteria)
      .subscribe(
        data => {
          console.log('getData', data);
          this.list = data.items;
          this.gridCriteria = data.gridCriteria;
          //this.pages = this.getPageArray();
        },
        error => {
          console.log('error', error);
        });
  }

  

}
