export class User {
    userId: string;
    firstName: string;
    lastName: string;
    token?: string;
    roleName: string;
    refreshToken?: string;
    tokenTimeoutMins: number;
    appMenu: AppMenu[];
}

export class AppMenu {
    code: string;
    name: string;
    description: string;
    icon: string;
    path: string;
    subMenu: SubMenu[];
}

export class SubMenu {
    code: string;
    name: string;
    description: string;
    icon: string;
    path: string;
}