export class RoleModel {
    id: number;
    name: string;
    description: string;
    createBy: string;
    createDate?: Date;
    updateBy: string;
    updateDate?: Date;
    
}
