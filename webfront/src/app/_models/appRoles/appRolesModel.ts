
export class AppRoleModel {
    id: number;
    name: string;
    description: string;
    createBy: string;
    createDate?: Date;
    updateBy: string;
    updateDate?: Date;
    appModuleGroup: AppModuleGrouModel[];
}

export class AppModuleGrouModel {
    code: string;
    name: string;
    itemSort?: number;
    appModule: AppModuleModel[];
}

export class AppModuleModel {
    appRoleModuleId: bigint;
    code: string;
    mainModuleCode: string;
    name: string;
    description: string;
    editableSetting: boolean;
    plantScopeSetting: boolean;
    icon: string;
    itemSort?:number;
    isAuthorize: boolean;
    isEditAble: boolean;
    authorizePlants: PlantModel[];
}

export class PlantModel {
    code: string;
    description: string;
    isAuthorize: boolean;
    itemSort?: number;
}