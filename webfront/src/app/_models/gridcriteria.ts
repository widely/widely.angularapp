export class GridCriteria {
    pageSize: number;
    page: number;
    sortby: string;
    sortdir: string;
    totalPage :number;
    totalRecord :number;
}