import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { GridCriteria } from '../../_models/gridcriteria';

@Injectable({ providedIn: 'root' })
export class RoleService  {

    constructor(private http: HttpClient) { }
    ngOnInit() { }

    getData(Keyword:string , gridCriteria :GridCriteria ) {
        return this.http.post<any>(`${environment.authentication}/api/AppRole/list2`, { Keyword : Keyword, gridCriteria : gridCriteria });
    }

    getDataInputForm() {
        return this.http.post<any>(`${environment.authentication}/api/AppRole/input-form`, {}).pipe(
            map((data) => {
                return data;
            })
        );
    }
}