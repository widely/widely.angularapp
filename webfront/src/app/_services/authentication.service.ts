import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

import { User } from '../_models/user';

import { environment } from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;

    constructor(
        private http: HttpClient,
        private router: Router) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public get getCurrentUser() {
        return JSON.parse(localStorage.getItem('currentUser'));
    }


    login(username: string, password: string) {
        return this.http.post<any>(`${environment.authentication}/api/auth/login`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                const userResult = user.data;
                localStorage.setItem('currentUser', JSON.stringify(userResult));
                this.currentUserSubject.next(userResult);
                this.startRefreshTokenTimer();
                return user;
            }));
    }

    logout() {
        if (this.currentUserValue) {
            this.http.post<any>(`${environment.authentication}/api/auth/revoke-token`, {
                Token: this.currentUserValue.refreshToken
            }).subscribe();
        }
        this.stopRefreshTokenTimer();
        this.currentUserSubject.next(null);
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }

    autoLogout() {
        this.stopRefreshTokenTimer();
        this.currentUserSubject.next(null);
        localStorage.removeItem('currentUser');
        this.router.navigate(['/login']);
    }

    // logout() {
    //     // remove user from local storage to log user out
    //     localStorage.removeItem('currentUser');
    //     this.currentUserSubject.next(null);
    // }

    refreshToken() {
        return this.http.post<any>(`${environment.authentication}/api/auth/refresh-token`, {
            Token: this.currentUserValue.refreshToken
        })
            .pipe(
                map((user) => {
                    const userResult = user.data;
                    localStorage.setItem('currentUser', JSON.stringify(userResult));
                    this.currentUserSubject.next(user.data);
                    this.startRefreshTokenTimer();
                    return user;
                }));
    }

    verifyRefreshToken() {
        return this.http.post<any>(`${environment.authentication}/api/auth/verify-refresh-token`, {
            Token: this.currentUserValue.refreshToken
        })
            .pipe(map((user) => {
                const userResult = user.data;
                localStorage.setItem('currentUser', JSON.stringify(userResult));
                this.currentUserSubject.next(user.data);
                this.startRefreshTokenTimer();
                return user;
            }));
    }

    // helper methods

    private refreshTokenTimeout;

    private startRefreshTokenTimer() {
        // parse json object from base64 encoded jwt token
        // const jwtToken = JSON.parse(atob(this.currentUserValue.token.split('.')[1]));

        // alert(this.currentUserValue.tokenTimeoutMins);

        // // set a timeout to refresh the token a minute before it expires
        // const expires = new Date(jwtToken.exp * 1000);
        // const timeout = expires.getTime() - Date.now() - (60 * 1000);
        // this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);

        const tokenTimeout = (this.currentUserValue.tokenTimeoutMins * 60 * 1000);
        console.log('timeoutMillisecond', tokenTimeout);
        this.refreshTokenTimeout = setTimeout(() => setTimeout(() => this.refreshToken().subscribe(), tokenTimeout));
    }

    private stopRefreshTokenTimer() {
        clearTimeout(this.refreshTokenTimeout);
    }

    public editablePermission(appModuleCode: string) {
        const jwtToken = JSON.parse(atob(this.currentUserValue.token.split('.')[1]));
        const appModuleJSON = JSON.parse(jwtToken.appauthorize);
        const myModule = appModuleJSON.find(object => object.Code === appModuleCode);
        return myModule.Editable;
    }
}