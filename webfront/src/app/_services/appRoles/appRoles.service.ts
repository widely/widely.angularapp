import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { AppRoleModel, AppModuleGrouModel, AppModuleModel } from '../../_models/appRoles/appRolesModel';



@Injectable({ providedIn: 'root' })
export class AppRolesService  {

    constructor(private http: HttpClient) { }

    ngOnInit() { }

    getData(roleName: string) {
        return this.http.post<any>(`${environment.authentication}/api/AppRole/list`, { roleName });
    }

    getInputForm() {
        return this.http.post<any>(`${environment.authentication}/api/AppRole/input-form`, {})
        .pipe(map((data) => {
                return data;
            })
        );
    }

    addInputForm(formData: any) {
        console.log('formData', formData);
        return this.http.post<any>(`${environment.authentication}/api/AppRole/add`, formData)
            .pipe(map(resp => {
                return resp;
            }));
    }
}