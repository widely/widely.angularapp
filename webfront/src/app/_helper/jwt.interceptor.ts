import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_services';
import { environment } from '../../environments/environment';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     // add authorization header with jwt token if available
    //     let localCurrentUser = this.authenticationService.getCurrentUser;
    //     if (localCurrentUser && localCurrentUser.data) {
    //     console.log('currentUser',localCurrentUser);
    //         request = request.clone({
    //             setHeaders: {
    //                 Authorization: `Bearer ${localCurrentUser.data.token}`
    //             }
    //         });
    //     }


    //     // let currentUser = this.authenticationService.currentUserValue;
    //     // console.log('currentUser',currentUser);
    //     // if (currentUser) {
    //     //     request = request.clone({
    //     //         setHeaders: {
    //     //             Authorization: `Bearer ${currentUser}`
    //     //         }
    //     //     });
    //     // }
    //     return next.handle(request);
    // }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to the api url
        const user = this.authenticationService.currentUserValue;
        const isLoggedIn = user && user.token;
        const isApiUrl = request.url.startsWith(environment.authentication);
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: { Authorization: `Bearer ${user.token}` }
            });
        }

        return next.handle(request);
    }
}