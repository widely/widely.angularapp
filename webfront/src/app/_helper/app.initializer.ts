
import { AuthenticationService } from '../_services';
import { Router } from '@angular/router';

export function appInitializer(authenticationService: AuthenticationService, router: Router) {
    if (authenticationService.currentUserValue) {
        return () => new Promise(resolve => {
            // attempt to refresh token on app start up to auto authenticate
            // authenticationService.refreshToken().subscribe().add(resolve);
            authenticationService.verifyRefreshToken().subscribe(
                res => {
                    if (!res.data) {
                        return () => authenticationService.autoLogout();
                    } else {
                        return res;
                    }
                }
            ).add(resolve);
        });
    } else {
        return () => authenticationService.autoLogout();
    }
}