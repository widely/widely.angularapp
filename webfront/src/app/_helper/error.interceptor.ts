import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthenticationService } from '../_services';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(
        private authenticationService: AuthenticationService
        , private router: Router
        , private toastr: ToastrService
    ) { }

    // intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //     return next.handle(request).pipe(catchError(err => {
    //         if (err.status === 401) {
    //             // auto logout if 401 response returned from api
    //             this.authenticationService.logout();
    //             location.reload(true);
    //         }

    //         const error = err.error.message || err.statusText;
    //         return throwError(error);
    //     }));
    // }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            if ([401].includes(err.status)) {
                // auto logout if 401 response returned from api
                this.authenticationService.autoLogout();
            }
            // else {
            //     this.router.navigate(['/404'], { queryParams: { errStatus: err.status, errMassage: err.error || err.error.message } });
            // }
            // const error = (err && err.error && err.error.message) || err.statusText;
            const error = err.error;
            // this.toastr.error(error, 'Error System !');
            return throwError(error);
        }));
    }
}