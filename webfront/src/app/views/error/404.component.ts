import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: '404.component.html'
})
export class P404Component implements OnInit {
  errorStatus: string;
  errorMessage: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.errorStatus = this.route.snapshot.queryParams['errStatus'] == '0' ? '404' : this.route.snapshot.queryParams['errStatus'] || '404';
    this.errorMessage = this.route.snapshot.queryParams['errMassage'] || 'Not found.';
  }

}
