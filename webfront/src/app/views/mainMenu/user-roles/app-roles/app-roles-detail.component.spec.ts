import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppRolesDetailComponent } from './app-roles-detail.component';

describe('AppRolesDetailComponent', () => {
  let component: AppRolesDetailComponent;
  let fixture: ComponentFixture<AppRolesDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppRolesDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppRolesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
