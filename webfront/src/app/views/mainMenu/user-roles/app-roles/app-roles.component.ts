import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppRolesService } from '../../../../_services/appRoles/appRoles.service';
import { AppRoleModel } from '../../../../_models/appRoles/appRolesModel';
import { User } from '../../../../_models';
import { AuthenticationService } from '../../../../_services';


@Component({
  selector: 'app-app-roles',
  templateUrl: './app-roles.component.html',
  styleUrls: ['./app-roles.component.css']
})
export class AppRolesComponent implements OnInit {
  appRoles: AppRoleModel[];
  masterRole: string;
  user: User;
  editable: boolean;
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';

  constructor(
    private appRolesService: AppRolesService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.editable = this.authenticationService.editablePermission('ROLES');
  }

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
    this.appRolesService.getData(this.user.roleName)
      .subscribe(
        data => {
          // console.log('getData', data);
          this.appRoles = data;
        },
        error => {
          console.log('error', error);
        });


  }

  onAdd() {
    this.router.navigate(['../AppRoles/Add'], { relativeTo: this.route });
  }

  onEdit(item: AppRoleModel) {
    console.log(item);
  }

  onDelete(item: AppRoleModel) {
    console.log(item);
  }

  

  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

}
