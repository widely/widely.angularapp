import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../../../_services';
import { AppRoleModel, AppModuleGrouModel, AppModuleModel } from '../../../../_models/appRoles/appRolesModel';
import { AppRolesService } from '../../../../_services/appRoles/appRoles.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-app-roles-detail',
  templateUrl: './app-roles-detail.component.html',
  styleUrls: ['./app-roles-detail.component.css']
})
export class AppRolesDetailComponent implements OnInit {
  isCollapsed: boolean = false;
  iconCollapse: string = 'icon-arrow-up';
  submitted = false;
  loading = false;
  error = '';

  alert = undefined;
  pnotifyInfo = undefined;
  pnotifyConfirm = undefined;

  appRoles: AppRoleModel;
  appModuleGroup: AppModuleGrouModel[];
  appModule: AppModuleModel[];

  detailForm: FormGroup;


  constructor(
    private router: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private appRolesService: AppRolesService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) {
  }

  ngOnInit(): void {
    this.testLoading(); // ทดสอบ loading spinner
    this.getAppRoles();
    console.log('appModuleGroup', this.appModuleGroup);

    this.detailForm = this.formBuilder.group({
      Name: ['', Validators.required],
      Description: ['', Validators.required]
    });

  }

  testLoading() {
    /** spinner starts on init */
    this.spinner.show();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    }, 5000);
  }

  showSuccess() {
    this.toastr.success('Toastr fun!');
  }

  get formControl() { return this.detailForm.controls; }

  onSubmit() {
    this.spinner.show();
    const formResult = { ...this.detailForm.value };
    this.submitted = true;

    // stop here if form is invalid
    if (this.detailForm.invalid) {
      return;
    }
    this.loading = true;
    this.appRolesService.addInputForm(formResult)
      .pipe(first())
      .subscribe({
        next: res => {
          this.loading = false;
          this.spinner.hide();
          if (res.success === true) { this.toastr.success('บันทึกข้อมูลสำเร็จ'); }
          else { this.toastr.error(res.message, 'Error'); }
        },
        error: error => {
          this.error = error;
          this.loading = false;
          this.spinner.hide();
          this.toastr.error(error, 'Error');
        }
      });

  }

  getAppRoles() {
    return this.appRolesService.getInputForm().subscribe(
      data => {
         this.appRoles = data;
         this.appModuleGroup = this.appRoles.appModuleGroup;
         console.log('this.appModuleGroup', this.appModuleGroup);
        },
      err => {
        console.log('error', err);
      }
    );
  }



  collapsed(event: any): void {
    // console.log(event);
  }

  expanded(event: any): void {
    // console.log(event);
  }

  toggleCollapse(): void {
    this.isCollapsed = !this.isCollapsed;
    this.iconCollapse = this.isCollapsed ? 'icon-arrow-down' : 'icon-arrow-up';
  }

}
