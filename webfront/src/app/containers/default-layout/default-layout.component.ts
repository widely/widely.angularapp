import { Component, OnInit} from '@angular/core';
import { navItems, _navItems } from '../../_nav';

import { AuthenticationService } from '../../_services';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.css']
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = _navItems;
  public currentMenu = this.authenticationService.currentUserValue.appMenu;


  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    if (this.navItems.length === 0) {
      this.renderMenu();
    }
  }

  ngOnInit() {
    // console.log('renderMenu', this.authenticationService.currentUserValue.appMenu);
  }

  renderMenu() {
    _navItems.push({ name: 'Home', url: '/Home', icon: 'icon-home' }); // home menu

    this.currentMenu.forEach(main => {
      _navItems.push({ name: main.name, title: true });

      main.subMenu.forEach(sub => {
        _navItems.push({ name: sub.name, url: sub.path, icon: sub.icon });
      });
    });
  }


  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }


}
