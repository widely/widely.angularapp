﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AuthenApi.BusinessEntities
{
    [Owned]
    public class RefreshToken
    {
        [Key]
        [JsonIgnore]
        public int Id { get; set; }

        public string Token { get; set; }
        public DateTime Expires { get; set; }
        public bool IsExpired => DateTime.UtcNow >= Expires;
        public DateTime Created { get; set; }
        public string CreatedByIp { get; set; }
        public string UserAgent { get; set; }
        //public DateTime? Revoked { get; set; }
        //public string RevokedByIp { get; set; }
        //public string ReplacedByToken { get; set; }
        //public bool IsActive => Revoked == null && !IsExpired;
    }

    public class RevokeTokenRequest
    {
        public string Token { get; set; }
    }

    public class RefreshTokenRequest
    {
        public string Token { get; set; }
    }
}
