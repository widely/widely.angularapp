﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenApi.BusinessEntities
{
    public class LoginUserEntity
    {
        public string Username { get; set; }
        public int? RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsForceChangePassword { get; set; }
        public int? LoginAttemptCount { get; set; }
        public DateTime LastLogin { get; set; }
        public DateTime? LastChangePassword { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public DateTime TokenExpire { get; set; }
        public int TokenTimeoutMins { get; set; }

        public List<LoginAppMenu> AppMenu { get; set; }

        //public List<LoginAppmoduleEntity> AppModule { get; set; }

    }

    public class LoginAppMenu
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Path { get; set; }

        public List<LoginAppMenu> SubMenu { get; set; }
    }
    public class AuthorizeAppmoduleEntity
    {
        public long AppRoleModuleId { get; set; }
        public string Code { get; set; }
        public string MainModuleCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Editable { get; set; }
        public string Icon { get; set; }
        public string Path { get; set; }
        public bool PlantScopeSetting { get; set; }
        public int? ItemSort { get; set; }
        public List<AuthorizePlantEntity> AuthorizePlants { get; set; }
       

    }

    

    public class AuthorizePlantEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }

    
}
