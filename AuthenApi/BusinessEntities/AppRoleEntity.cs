﻿using AuthenApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenApi.BusinessEntities
{
    public class AppRoleEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public List<AppModuleGroupEntity> AppModuleGroup { get; set; }
    }

    public class AppRoleFilterEntity
    {
        public string Keyword { get; set; }
        public GridCriteria gridCriteria { get; set; }
    }

    public class AppModuleGroupEntity
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int? ItemSort { get; set; }
        public List<AppModuleEntity> AppModule { get; set; }
    }

    public class AppModuleEntity
    {
        public long AppRoleModuleId { get; set; }
        public string Code { get; set; }
        public string MainModuleCode { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool EditableSetting { get; set; }
        public bool PlantScopeSetting { get; set; }
        public string Icon { get; set; }
        public int? ItemSort { get; set; }
        public bool IsAuthorize { get; set; }
        public bool? IsEditAble { get; set; }
        public List<PlantEntity> AuthorizePlants { get; set; }
    }

    public class PlantEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public bool IsAuthorize {get;set;}
        public int? ItemSort { get; set; }
    }


}
