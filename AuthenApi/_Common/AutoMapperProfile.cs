using AuthenApi.DataContext;
using AuthenApi.BusinessEntities;
using AutoMapper;

namespace AuthenApi
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile ()
        {
            CreateMap<DataContext.AppUser, LoginUserEntity>();
            CreateMap<DataContext.AppRole, AppRoleEntity>();
            CreateMap<DataContext.AppUserRefreshToken, RefreshToken>();
            CreateMap<RefreshToken, DataContext.AppUserRefreshToken>();
        }
    }
}