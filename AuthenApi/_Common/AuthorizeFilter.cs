﻿using AuthenApi.DataContext;
using AuthenApi.BusinessEntities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AuthenApi
{
    
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        public AuthorizeAttribute(string item, bool action)
        : base(typeof(AuthorizeActionFilter))
        {
            Arguments = new object[] { item, action };
        }
    }

    public class AuthorizeActionFilter : IAuthorizationFilter
    {
        private readonly string _AppModule;
        private readonly bool _Editable;
        public AuthorizeActionFilter(string AppModule, bool EditablePermission)
        {
            _AppModule = AppModule;
            _Editable = EditablePermission;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (_AppModule == "ALLALLOW") return;
            try
            {

                var appauthorize = JsonConvert.DeserializeObject<List<AuthorizeAppmoduleEntity>>(context.HttpContext.User.Claims
                    .FirstOrDefault(x => x.Type == "appauthorize").Value);


                bool isAuthorized = HasAuthorize(appauthorize, _AppModule, _Editable); // :)

                if (!isAuthorized)
                {
                    context.Result = new ForbidResult();
                }
            }
            catch
            {
                context.Result = new UnauthorizedResult();
            }
        }

        private bool HasAuthorize(List<AuthorizeAppmoduleEntity> appauthorize, string AppModuleCode , bool EditablePermission)
        {
            bool hasAuthorizeAccess = appauthorize.Any(x => x.Code == AppModuleCode);
            if (!hasAuthorizeAccess) return false;
            if(EditablePermission==true)
            {
                var appModule = appauthorize.FirstOrDefault(x => x.Code == AppModuleCode);
                if (appModule.Editable == true) return true; 
                else return false;
            }
            else
            {
                return true;
            }
        }

    }
}
