﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenApi.BusinessServices.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AuthenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : _AuthorizeBaseController
    {
        private readonly IUserProfileRepository _serviceRepo;
        public UserProfileController(IHttpContextAccessor httpContextAccessor, IUserProfileRepository serviceRepo) : base(httpContextAccessor)
        {
            _serviceRepo = serviceRepo;
        }


        [HttpPost("getinfo")]
        [Authorize("ALLALLOW", false)]
        public async Task<IActionResult> GetCurrentUserInfo()
        {
            try
            {
              
                var username = GetUserName();
                var result = await _serviceRepo.GetLoginUser(username);
                return Ok(result);
            }
            catch (Exception e)                        
            {
                //LogException(e);
                return StatusCode(500);
            }
        }
    }
}