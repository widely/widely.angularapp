﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using AuthenApi.BusinessEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AuthenApi.Controllers
{

    public class _AuthorizeBaseController : ControllerBase
    {
        public readonly IHttpContextAccessor _httpContextAccessor;

        public string GetUserName() => _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
        public List<AuthorizeAppmoduleEntity> GetCurrentAuthorizeModule() => JsonConvert.DeserializeObject<List<AuthorizeAppmoduleEntity>>(_httpContextAccessor.HttpContext.User.Claims
                    .FirstOrDefault(x => x.Type == "appauthorize").Value);
        
        public _AuthorizeBaseController(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }
    }
}