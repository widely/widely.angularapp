﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenApi.BusinessServices.Interface;
using AuthenApi.DataContext;
using AuthenApi.BusinessEntities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AuthenApi.Controllers
{
    //[EnableCors("AllowAll")]
    [Route("api/[controller]")]
    [ApiController]
    public class AppRoleController : _AuthorizeBaseController
    {

        private readonly IAppRoleRepository _serviceRepo;
        public AppRoleController(IHttpContextAccessor httpContextAccessor, IAppRoleRepository serviceRepo) : base(httpContextAccessor)
        {
            _serviceRepo = serviceRepo;
        }

        [HttpPost("list")]
        [Authorize("ROLES", false)]
        public async Task<IActionResult> GetAllRole(string RoleName)
        {
            try
            {
                var roles = await _serviceRepo.GetAll(RoleName);
                return Ok(roles);
            }
            catch(Exception e)
            {
                //LogException(e);
                return StatusCode(500,e);
            }
        }

        [HttpPost("list2")]
        [Authorize("ROLES", false)]
        public async Task<IActionResult> GetAllRole2(AppRoleFilterEntity filter)
        {
            try
            {
                var roles = await _serviceRepo.GetListByFilter(filter);
                return Ok(roles);
            }
            catch (Exception e)
            {
                //LogException(e);
                return StatusCode(500, e);
            }
        }

        [HttpPost("getbyid")]
        [Authorize("ROLES", false)]
        public async Task<IActionResult> GetAllRole(int id)
        {
            try
            {
                var role = await _serviceRepo.GetById(id);
                return Ok(role);
            }
            catch (Exception e)
            {
                //LogException(e);
                return StatusCode(500,e);
            }
        }

        [HttpPost("input-form")]
        [Authorize("ROLES", false)]
        public async Task<IActionResult> GetInputForm()
        {
            try
            {
                var role = await _serviceRepo.GetFormModel();
                return Ok(role);
            }
            catch (Exception e)
            {
                //LogException(e);
                return StatusCode(500);
            }
        }


        [HttpPost("add")]
        [Authorize("ROLES", true)]
        public async Task<IActionResult> AddRole(AppRoleEntity role)
        {
            try
            {
                ServiceResponse<AppRoleEntity> response = await _serviceRepo.Add(role);
                return Ok(response);
            }
            catch (Exception e)
            {
                //LogException(e);
                string msg = e.Message.ToString();
                return BadRequest(msg);
            }
        }


        [HttpPost("edit")]
        [Authorize("ROLES", true)]
        public async Task<IActionResult> EditRole(AppRoleEntity role)
        {
            try
            {
                ServiceResponse<AppRoleEntity> response = await _serviceRepo.Edit(role);
                return Ok(response);
            }
            catch (Exception e)
            {
                //LogException(e);
                return StatusCode(500);
            }
        }

        [HttpPost("delete")]
        [Authorize("ROLES", true)]
        public async Task<IActionResult> DeleteRole(AppRoleEntity role)
        {
            try
            {
                ServiceResponse<AppRoleEntity> response = await _serviceRepo.Delete(role);
                return Ok(response);
            }
            catch (Exception e)
            {
                //LogException(e);
                return StatusCode(500);
            }
        }
    }
}