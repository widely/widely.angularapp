﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AuthenApi.BusinessEntities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AuthenApi.Controllers
{
    
    [ApiController]
    [Route("api/[controller]")]
    public class TestController :  _AuthorizeBaseController
    {
        public TestController(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor)
        {

        }
        [Authorize("ROLES", true)]
        [HttpGet]
        public List<AuthorizeAppmoduleEntity> Get()
        {
            var current_authorize_module = GetCurrentAuthorizeModule();
            return current_authorize_module;
        }
    }
}
