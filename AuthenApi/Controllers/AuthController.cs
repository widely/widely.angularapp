﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthenApi.BusinessServices.Interface;
using AuthenApi.DataContext;
using AuthenApi.BusinessEntities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace AuthenApi.Controllers
{
    //[EnableCors("AllowAll")]
    [ApiController]
    [Route("api/[controller]")]
 
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepo;

        public AuthController(IAuthRepository authRepository)
        {
            _authRepo = authRepository;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(AuthenticateRequest request)
        {
            ServiceResponse<LoginUserEntity> response = await _authRepo.Login(
                request.Username, request.Password, ipAddress() , userAgent()
            );

            if (!response.Success)
            {
                return Unauthorized(response);
            }
            setTokenCookie(response.Data.RefreshToken);
            return Ok(response);
        }


        [HttpPost("refresh-token")]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest model)
        {
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return Unauthorized(new { message = "Token is required" });

            var response = await _authRepo.RefreshToken(token, ipAddress(), userAgent());

            if (response == null)
                return Unauthorized(new { message = "Invalid token" });

            setTokenCookie(response.Data.RefreshToken);

            return Ok(response);
        }

        [HttpPost("verify-refresh-token")]
        public async Task<IActionResult> VerifyRefreshToken([FromBody] RefreshTokenRequest model)
        {
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return Unauthorized(new { message = "Token is required" });

            var response = await _authRepo.VerifyRefreshToken(token, ipAddress(), userAgent());

            if (response == null)
                return Unauthorized(new { message = "Invalid token" });

            //setTokenCookie(response.Data.RefreshToken);

            return Ok(response);
        }

        [HttpPost("revoke-token")]
        public async Task<IActionResult> RevokeToken([FromBody] RevokeTokenRequest model)
        {
            // accept token from request body or cookie
            var token = model.Token ?? Request.Cookies["refreshToken"];

            if (string.IsNullOrEmpty(token))
                return Unauthorized(new { message = "Token is required" });

            var response = await _authRepo.RevokeToken(token, ipAddress());

            if (!response)
                return Unauthorized(new { message = "Token not found" });

            return Ok(new { message = "Token revoked" });
        }

        [HttpGet("{id}/refresh-tokens")]
        public async Task<IActionResult> GetRefreshTokens(int id)
        {
            var user = await _authRepo.GetLoginUserById(id);
            if (user == null) return NotFound();

            return Ok(user.AppUserRefreshToken);
        }


        // helper methods
        private void setTokenCookie(string token)
        {
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Secure = true,
                Expires = DateTime.UtcNow.AddDays(7)
            };
            Response.Cookies.Append("refreshToken", token, cookieOptions);
        }

        private string ipAddress()
        {
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
                return Request.Headers["X-Forwarded-For"];
            else
                return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
        }

        private string userAgent()
        {
            return Request.Headers["User-Agent"].ToString();
        }

    }
}
