﻿using AuthenApi.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenApi.BusinessServices.Interface
{
    public interface IUserProfileRepository
    {
        Task<LoginUserEntity> GetLoginUser(string  username);
    }
}
