﻿using AuthenApi.BusinessEntities;
using AuthenApi.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenApi.BusinessServices.Interface
{
    public interface IAppRoleRepository
    {
        Task<List<AppRoleEntity>> GetAll(string RoleName);
        Task<GridResult<AppRoleEntity>> GetListByFilter(AppRoleFilterEntity filter);
        Task<ServiceResponse<AppRoleEntity>> Add(AppRoleEntity approle);
        Task<ServiceResponse<AppRoleEntity>> Edit(AppRoleEntity approle);
        Task<ServiceResponse<AppRoleEntity>> Delete(AppRoleEntity approle);
        Task<AppRoleEntity> GetById(int Id);
        Task<AppRoleEntity> GetFormModel();
    }
}
