﻿using AuthenApi.BusinessEntities;
using AuthenApi.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenApi.BusinessServices.Interface
{
    public interface IAuthRepository
    {
        Task<ServiceResponse<LoginUserEntity>> Login(string username, string password ,string ipAddress, string userAgent);
        Task<ServiceResponse<LoginUserEntity>> RefreshToken(string token, string ipAddress, string userAgent);
        Task<ServiceResponse<LoginUserEntity>> VerifyRefreshToken(string token, string ipAddress, string userAgent);
        Task<bool> RevokeToken(string token, string ipAddress);
        Task<AppUser> GetLoginUserById(int Id);
    }
}
