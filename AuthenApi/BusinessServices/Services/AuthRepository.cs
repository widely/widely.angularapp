﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AuthenApi.BusinessServices.Interface;
using AuthenApi.DataContext;
using AuthenApi.BusinessEntities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using AuthenApi.Helpers;
using System.Security.Cryptography;

namespace AuthenApi.BusinessServices.Services
{
    public class AuthRepository : IAuthRepository
    {
        private readonly IMapper _mapper;
        private readonly QmreportContext _context;
        private readonly IConfiguration _configuration;

        public AuthRepository(IMapper mapper, QmreportContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            _mapper = mapper;
        }

        private DateTime tokenExpire;
        private int tokenAge;

        public async Task<ServiceResponse<LoginUserEntity>> Login(string username, string password, string ipAddress, string userAgent)
        {
            tokenAge = Convert.ToInt32(_configuration.GetSection("AppSettings:TokenExpHours").Value);
            tokenExpire = DateTime.Now.AddHours(tokenAge);

            ServiceResponse<LoginUserEntity> response = new ServiceResponse<LoginUserEntity>();
            try
            {
                var user = await _context.AppUser
                    .Include(x=>x.Role)
                    .FirstOrDefaultAsync(x => x.Username.ToLower().Equals(username.ToLower()) && x.IsActive==true);
                if (user == null)
                {
                    response.Success = false;
                    response.Message = "User not found.";
                }
                else if (!PasswordHashUtility.VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                {
                    response.Success = false;
                    response.Message = "Wrong password.";
                }
                else
                {
                    //login success
                    user.LastLogin = DateTime.Now;

                    var refreshToken = _mapper.Map<AppUserRefreshToken>(generateRefreshToken(ipAddress, userAgent));
                    // save refresh token
                    user.AppUserRefreshToken.Add(refreshToken);

                    _context.AppUser.Update(user);
                    await _context.SaveChangesAsync();
                    response.Data = GetLoginUserInfo(user);
                    response.Data.RefreshToken = refreshToken.Token;
                    response.Data.TokenExpire = tokenExpire;
                    response.Data.TokenTimeoutMins = ((int)tokenExpire.Subtract(DateTime.Now).TotalMinutes) - 1;

                }

            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = ex.Message;
            }

            return response;
        }
        public async Task<ServiceResponse<LoginUserEntity>> RefreshToken(string token, string ipAddress , string userAgent)
        {
            try
            {
                tokenAge = Convert.ToInt32(_configuration.GetSection("AppSettings:TokenExpHours").Value);
                tokenExpire = DateTime.Now.AddHours(tokenAge);

                var user = await _context.AppUser.Include(x => x.Role).Include(x => x.AppUserRefreshToken)
                .SingleOrDefaultAsync(u => u.AppUserRefreshToken.Any(t => t.Token == token));

                // return null if no user found with token
                if (user == null) return null;

                var refreshToken = user.AppUserRefreshToken.Single(x => x.Token == token);

                // return null if token is no longer active
                //bool IsActive = refreshToken.Revoked == null && !(DateTime.UtcNow >= refreshToken.Expires);
                //if (IsActive) return null;
                if (refreshToken == null) return null;

                // replace old refresh token with a new one and save
                var newRefreshToken = _mapper.Map<AppUserRefreshToken>(generateRefreshToken(ipAddress, userAgent));

                //refreshToken.Revoked = DateTime.UtcNow;
                //refreshToken.RevokedByIp = ipAddress;
                //refreshToken.ReplacedByToken = newRefreshToken.Token;
                user.AppUserRefreshToken.Add(newRefreshToken);
                _context.AppUserRefreshToken.Remove(refreshToken);
                
                _context.Update(user);
                _context.SaveChanges();

                ServiceResponse<LoginUserEntity> response = new ServiceResponse<LoginUserEntity>();
                response.Data = GetLoginUserInfo(user);
                response.Data.RefreshToken = newRefreshToken.Token;
                response.Data.TokenExpire = tokenExpire;
                response.Data.TokenTimeoutMins = ((int)tokenExpire.Subtract(DateTime.Now).TotalMinutes) - 1;
                return response;
            }
            catch (Exception e)
            {

                throw e;
            }
            
            //return new AuthenticateResponse(user, jwtToken, newRefreshToken.Token);
        }

        public async Task<ServiceResponse<LoginUserEntity>> VerifyRefreshToken(string token, string ipAddress, string userAgent)
        {
            try
            {
                tokenAge = Convert.ToInt32(_configuration.GetSection("AppSettings:TokenExpHours").Value);
                tokenExpire = DateTime.Now.AddHours(tokenAge);

                var user = await _context.AppUser.Include(x => x.Role).Include(x => x.AppUserRefreshToken)
                .SingleOrDefaultAsync(u => u.AppUserRefreshToken.Any(t => t.Token == token));
                // return null if no user found with token
                if (user == null) return null;

                var refreshToken = user.AppUserRefreshToken.Single(x => x.Token == token);
                if (refreshToken == null) return null;
                if (DateTime.Now >= refreshToken.Expires)
                {
                    _context.AppUserRefreshToken.Remove(refreshToken);
                    _context.SaveChanges();
                    return null;
                }

                // replace old refresh token with a new one and save
                var newRefreshToken = _mapper.Map<AppUserRefreshToken>(generateRefreshToken(ipAddress , userAgent));
                newRefreshToken.AppUserId = user.Id;
                //refreshToken.Revoked = DateTime.UtcNow;
                //refreshToken.RevokedByIp = ipAddress;
                //refreshToken.ReplacedByToken = newRefreshToken.Token;
                _context.AppUserRefreshToken.Add(newRefreshToken);
                _context.AppUserRefreshToken.Remove(refreshToken);
                _context.SaveChanges();

                ServiceResponse<LoginUserEntity> response = new ServiceResponse<LoginUserEntity>();
                response.Data = GetLoginUserInfo(user);
                response.Data.RefreshToken = newRefreshToken.Token;
                response.Data.TokenExpire = tokenExpire;
                response.Data.TokenTimeoutMins = ((int)tokenExpire.Subtract(DateTime.Now).TotalMinutes) - 1;
                return response;
            }
            catch (Exception e)
            {

                throw e;
            }

            //return new AuthenticateResponse(user, jwtToken, newRefreshToken.Token);
        }
        public async Task<bool> RevokeToken(string token, string ipAddress)
        {
            try
            {
                var user = await _context.AppUser.Include(t => t.AppUserRefreshToken)
                .FirstOrDefaultAsync(u => u.AppUserRefreshToken.Any(t => t.Token == token));

                // return false if no user found with token
                if (user == null) return false;

                var refreshToken = await _context.AppUserRefreshToken.FirstOrDefaultAsync(t => t.Token == token);
                if (refreshToken == null) return false;

                _context.Remove(refreshToken);
                await _context.SaveChangesAsync();

                //var refreshToken = user.AppUserRefreshToken.FirstOrDefault(x => x.Token == token);
                //user.AppUserRefreshToken.Remove(refreshToken);
                // return false if token is not active
                //bool IsActive = refreshToken.Revoked == null && !(DateTime.UtcNow >= refreshToken.Expires);
                //if (!IsActive) return false;

                // revoke token and save
                //refreshToken.Revoked = DateTime.UtcNow;
                //refreshToken.RevokedByIp = ipAddress;
                //_context.Update(user);
                //_context.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }
        public async Task<AppUser> GetLoginUserById(int Id)
        {
            var user =  await _context.AppUser.FindAsync(Id);
            return user;
        }
        private LoginUserEntity GetLoginUserInfo(AppUser user)
        {
            var userdto = _mapper.Map<LoginUserEntity>(user);
            userdto.RoleName = user.Role.Name;
            userdto.RoleDescription = user.Role.Description;

            var app_authorize_byrole = (from q in _context.AppRoleAppModule
                                        where q.AppRoleId == user.RoleId
                                        select new AuthorizeAppmoduleEntity
                                        {
                                            AppRoleModuleId = q.Id,
                                            Code = q.AppModuleCode,
                                            MainModuleCode = q.AppModuleCodeNavigation.MainModuleCode,
                                            ItemSort = q.AppModuleCodeNavigation.ItemSort,
                                            Name = q.AppModuleCodeNavigation.Name,
                                            Description = q.AppModuleCodeNavigation.Description,
                                            Editable = q.Editable,
                                            Icon = q.AppModuleCodeNavigation.Icon,
                                            Path = q.AppModuleCodeNavigation.Path,
                                            PlantScopeSetting = q.AppModuleCodeNavigation.PlantScopeSetting
                                        }).ToList();

            var app_authorize_include_mainmenu = (from q in _context.AppModule
                                                  where q.IsActive == true &&
                                                  (app_authorize_byrole.Select(x => x.Code).Contains(q.Code) ||
                                                  app_authorize_byrole.Select(x => x.MainModuleCode).Contains(q.Code))
                                                  select q).Distinct().ToList();

                    var appmoduledto = (from allapp in app_authorize_include_mainmenu
                                       join rolrapp in app_authorize_byrole
                                       on allapp.Code equals rolrapp.Code into j
                                       from t in j.DefaultIfEmpty()
                                       select new AuthorizeAppmoduleEntity
                                       {
                                           Code = allapp.Code,
                                           MainModuleCode = allapp.MainModuleCode,
                                           ItemSort = allapp.ItemSort,
                                           Name = allapp.Name,
                                           Description = allapp.Description,
                                           AppRoleModuleId = t == null ?0 : t.AppRoleModuleId,
                                           Editable = t==null? false : t.Editable,
                                           PlantScopeSetting = t==null? false : t.PlantScopeSetting,
                                           Path = allapp.Path,
                                           Icon = allapp.Icon

                                       }).ToList();


            //var appmoduledto
            foreach (var item in appmoduledto)
            {
                item.AuthorizePlants = new List<AuthorizePlantEntity>();
                var plants = from q in _context.AppRoleModulePlant.Include(x => x.PlantCodeNavigation)
                             where q.AppRoleModuleId == item.AppRoleModuleId
                             orderby q.PlantCodeNavigation.ItemSort
                             select q;
                foreach (var plant in plants)
                {
                    var plantdto = new AuthorizePlantEntity { Code = plant.PlantCode, Description = plant.PlantCodeNavigation.Description };
                    item.AuthorizePlants.Add(plantdto);
                }
            }

            var mainmenu = appmoduledto.Where(x => String.IsNullOrEmpty(x.MainModuleCode)).OrderBy(x => x.ItemSort)
                .Select(x => new LoginAppMenu
                {
                    Code = x.Code,
                    Name = x.Name,
                    Description = x.Description,
                    Icon = x.Icon,
                    Path = x.Path
                }).ToList();

            foreach (var item in mainmenu)
            {
                item.SubMenu = appmoduledto.Where(x => x.MainModuleCode == item.Code).OrderBy(x => x.ItemSort).Select(x => new LoginAppMenu
                {
                    Code = x.Code,
                    Name = x.Name,
                    Description = x.Description,
                    Icon = x.Icon,
                    Path = x.Path
                }).ToList();
            }

            userdto.AppMenu = mainmenu;
            //userdto.AppModule = appmoduledto;
            userdto.Token = this.CreateToken(userdto, appmoduledto);
            return userdto;
        }
        private string CreateToken(LoginUserEntity user , List<AuthorizeAppmoduleEntity> authorizeapp)
        {
            //var expired = Convert.ToInt32(_configuration.GetSection("AppSettings:TokenExpHours").Value);
            var appauthorize = JsonConvert.SerializeObject(authorizeapp);

            List<Claim> claims = new List<Claim> {
                new Claim (ClaimTypes.NameIdentifier, user.Username),
                new Claim (ClaimTypes.Role, user.RoleId.Value.ToString ()),
                new Claim("appauthorize", appauthorize)
            };

            SymmetricSecurityKey key = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value)
            );

            SigningCredentials creds = new SigningCredentials(
                key, SecurityAlgorithms.HmacSha512Signature
            );

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = tokenExpire,
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);

        }

        private RefreshToken generateRefreshToken(string ipAddress ,string userAgent)
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);
                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = tokenExpire,
                    Created = DateTime.Now,
                    CreatedByIp = ipAddress,
                    UserAgent = userAgent
                };
            }
        }

        
    }
}