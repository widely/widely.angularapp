﻿using AuthenApi.BusinessEntities;
using AuthenApi.BusinessServices.Interface;
using AuthenApi.DataContext;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthenApi.BusinessServices.Services
{
    public class UserProfileRepository : IUserProfileRepository
    {
        private readonly IMapper _mapper;
        private readonly QmreportContext _context;
        private readonly IConfiguration _configuration;

        public UserProfileRepository(IMapper mapper, QmreportContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            _mapper = mapper;
        }
        public async Task<LoginUserEntity> GetLoginUser(string username)
        {
            var user = await _context.AppUser
                    .Include(x => x.Role)
                    .FirstOrDefaultAsync(x => x.Username.ToLower().Equals(username.ToLower()) && x.IsActive == true);
            var userdto = _mapper.Map<LoginUserEntity>(user);
            userdto.RoleName = user.Role.Name;
            userdto.RoleDescription = user.Role.Description;
            return userdto;
        }


    }
}
