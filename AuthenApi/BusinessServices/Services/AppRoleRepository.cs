﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AuthenApi.BusinessServices.Interface;
using AuthenApi.DataContext;
using AuthenApi.BusinessEntities;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using AuthenApi.Helpers;

namespace AuthenApi.BusinessServices.Services
{
    public class AppRoleRepository : IAppRoleRepository
    {
        private readonly IMapper _mapper;
        private readonly QmreportContext _context;
        private readonly IConfiguration _configuration;

        public AppRoleRepository(IMapper mapper, QmreportContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
            _mapper = mapper;
        }

        public async Task<ServiceResponse<AppRoleEntity>> Add(AppRoleEntity approle_input)
        {
            ServiceResponse<AppRoleEntity> response = new ServiceResponse<AppRoleEntity>();
            try
            {
                var now = DateTime.Now;
                var approle = new AppRole
                {
                    Name = approle_input.Name,
                    Description = approle_input.Description,
                    CreateBy = approle_input.CreateBy,
                    CreateDate = now,
                    UpdateBy = approle_input.UpdateBy,
                    UpdateDate = approle_input.UpdateDate,
                };
                _context.AppRole.Add(approle);

                if (approle_input.AppModuleGroup.Count() > 0)
                {
                    foreach (var moduleGroup in approle_input.AppModuleGroup)
                    {
                        foreach (var module in moduleGroup.AppModule.Where(x => x.IsAuthorize == true))
                        {
                            var approle_module = new AppRoleAppModule
                            {
                                AppRoleId = approle.Id,
                                AppModuleCode = module.Code,
                                Editable = module.IsEditAble.HasValue ? module.IsEditAble.Value : false,
                                UpdateBy = approle_input.UpdateBy,
                                UpdateDate = now
                            };
                            _context.AppRoleAppModule.Add(approle_module);
                            foreach (var plant in module.AuthorizePlants.Where(x => x.IsAuthorize == true))
                            {
                                var rolemodule_plant = new AppRoleModulePlant
                                {
                                    AppRoleModuleId = approle_module.Id,
                                    PlantCode = plant.Code,
                                    UpdateBy = approle_input.UpdateBy,
                                    UpdateDate = now
                                };
                                _context.AppRoleModulePlant.Add(rolemodule_plant);
                            }
                        }
                    }

                }


                await _context.SaveChangesAsync();
                approle_input.Id = approle.Id;
                response.Success = true;
                response.Data = approle_input;
            }
            catch(Exception e)
            {
                while (e.InnerException != null) e = e.InnerException;
                response.Success = false;
                response.Message = e.Message;
                response.Data = approle_input;
                throw new Exception(response.Message);
            }
            return response;
        }

        public Task<ServiceResponse<AppRoleEntity>> Delete(AppRoleEntity approle)
        {
            throw new NotImplementedException();
        }

        public Task<ServiceResponse<AppRoleEntity>> Edit(AppRoleEntity approle)
        {
            throw new NotImplementedException();
        }

        public async Task<GridResult<AppRoleEntity>> GetListByFilter(AppRoleFilterEntity filter)
        {
            var filterData = await _context.AppRole.ToListAsync();
            if (!string.IsNullOrEmpty(filter.Keyword)) filterData = filterData
                   .Where(x => x.Name.ToLower().Contains(filter.Keyword.ToLower()) || x.Description.ToLower().Contains(filter.Keyword.ToLower())).ToList();
            var TotalRecord = filterData == null ? 0 : filterData.Count();
            var TotalPage = (TotalRecord + filter.gridCriteria.pageSize - 1) / filter.gridCriteria.pageSize;
            if (filter.gridCriteria !=null && filter.gridCriteria.Take > 0)
            {
                filter.gridCriteria.TotalItems = TotalRecord;
                filter.gridCriteria.TotalPage = (TotalRecord + filter.gridCriteria.pageSize - 1) / filter.gridCriteria.pageSize;
                if (!String.IsNullOrEmpty(filter.gridCriteria.sortby) && (!String.IsNullOrEmpty(filter.gridCriteria.sortdir)))
                {
                    if (filter.gridCriteria.sortdir == "desc")
                        filterData = filterData.OrderByDescending(filter.gridCriteria.sortby).ToList();
                    else
                        filterData = filterData.OrderBy(filter.gridCriteria.sortby).ToList();

                }
                filterData = filterData.Skip(filter.gridCriteria.skip).Take(filter.gridCriteria.Take).ToList();
            }

            var mapdata = _mapper.Map<List<AppRoleEntity>>(filterData);

            return new GridResult<AppRoleEntity>()
            {
                Items = mapdata,
                gridCriteria = new GridPageResult {
                    page = filter.gridCriteria.page,
                    pageSize = filter.gridCriteria.pageSize,
                    totalPage = TotalPage,
                    totalRecord = TotalRecord,
                    sortby = filter.gridCriteria.sortby,
                    sortdir = filter.gridCriteria.sortdir,
                }
            };
        }

        public async Task<List<AppRoleEntity>> GetAll(string RoleName)
        {
            
            var approles = await _context.AppRole.ToListAsync() ;
            if (!string.IsNullOrEmpty(RoleName)) approles = approles.Where(x => x.Name.Contains(RoleName)).ToList();
            var approlesdto = _mapper.Map<List<AppRoleEntity>>(approles);

            return approlesdto;
        }

        public async Task<AppRoleEntity> GetById(int Id)
        {

            var approle = await _context.AppRole.FirstOrDefaultAsync(x => x.Id== Id);
            var approlesdto = _mapper.Map<AppRoleEntity>(approle);

            return approlesdto;
        }

        public async Task<AppRoleEntity> GetFormModel()
        {
            var appModuleGroup = await _context.AppModule.Where(x => x.IsActive==true && string.IsNullOrEmpty(x.MainModuleCode)).OrderBy(x=>x.ItemSort)
                .Select(x=> new AppModuleGroupEntity
                {
                    Code = x.Code,
                    Name = x.Name,
                    ItemSort = x.ItemSort,
                }).ToListAsync();

            var plants = await _context.Plant.OrderBy(x=>x.ItemSort).
                Select(x=>new PlantEntity { Code =  x.Code, Description  = x.Description ,ItemSort = x.ItemSort , IsAuthorize = false}).ToListAsync();

            foreach (var item in appModuleGroup)
            {
                var appModule = await _context.AppModule.Where(x => x.IsActive == true && x.MainModuleCode == item.Code).OrderBy(x => x.ItemSort).OrderBy(x => x.ItemSort)
                    .Select(x => new AppModuleEntity
                    {
                        Code = x.Code,
                        Name = x.Name,
                        ItemSort = x.ItemSort,
                        EditableSetting = x.EditableSetting,
                        PlantScopeSetting = x.PlantScopeSetting,
                        IsAuthorize = false,
                        IsEditAble = false,
                        AuthorizePlants = plants
                    }).ToListAsync();
                item.AppModule = appModule;
            }
            AppRoleEntity result = new AppRoleEntity
            {
                Id = 0,
                Name = "",
                Description = "",
                AppModuleGroup = appModuleGroup
            };
            return result;
        }
    }
}
